Create a fat jar
```
mvn clean package
```

Start storm topology
```
storm jar target/storm-csv-1-jar-with-dependencies.jar  storm.kafka.KafkaDataProcessTopology   localhost:2181

(localhost:2181 is zookeeper )
```

Submit csv to storm producer
```
java -cp target/storm-csv-1-jar-with-dependencies.jar  storm.kafka.tools.StormProducer 127.0.0.1:9092 /tmp/1.csv
```

/tmp/1.csv is a sample csv with two column , check in main repo for reference

