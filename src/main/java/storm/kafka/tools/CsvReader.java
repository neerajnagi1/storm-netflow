package storm.kafka.tools;
 
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
 
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
public class CsvReader {
 
    public  List<Map<String,String>> read(String csvPath) throws FileNotFoundException, IOException {
         
        //Create the CSVFormat object
        CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
         
        //initialize the CSVParser object
        CSVParser parser = new CSVParser(new FileReader(csvPath), format);
        
        List<Map<String, String>> emps = new ArrayList<Map<String,String>>();
        Map<String, String> emp = new HashMap<String, String>();
        for(CSVRecord record : parser){
        	emp = record.toMap();
           
            System.out.println(record.toMap());
            emps.add(emp);
            
        }
        //close the parser
        parser.close();
         
        System.out.println(emps);
         
        //CSV Write Example using CSVPrinter
        return emps;
    }
 
}