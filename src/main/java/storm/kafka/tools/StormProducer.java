package storm.kafka.tools;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.util.Properties;
import storm.kafka.tools.CsvReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StormProducer {



    public static void main(String args[]) throws InterruptedException {
        Properties props = new Properties();

        props.put("metadata.broker.list", args[0]);
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        props.put("request.required.acks", "1");
        ProducerConfig config = new ProducerConfig(props);
        Producer<String, String> producer = new Producer<String, String>(config);
        CsvReader cr = new CsvReader();
        List<Map<String,String>> lines=null;
        try{
         lines = cr.read(args[1]);
        }catch(Exception e){
        	System.out.println("exception occurred "+ e);
        }
            for (Map<String,String> line : lines) {
                KeyedMessage<String, String> data = new KeyedMessage<String, String>("csv", line.get("Name")+"_"+line.get("Salary"));
                producer.send(data);
                Thread.sleep(10);
            }
      

    }
}
