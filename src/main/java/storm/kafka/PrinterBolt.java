package storm.kafka;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;

import backtype.storm.tuple.Tuple;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Map;

import org.apache.log4j.Logger;

 public class PrinterBolt extends BaseBasicBolt {
	 
	 //PrintWriter writer = null;
	 BufferedWriter writer = null;
	 @Override
	   public void prepare(Map conf, TopologyContext context){
		   try{
		   //writer = new PrintWriter("/tmp/salary.log", "UTF-8");
			   writer = new BufferedWriter(new FileWriter("/tmp/salary.log"));
		   }catch(Exception e){
			   System.out.println("exception occurred -> "+e);
		   }
			
	   }
	 
        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
        }

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
        	try{
        		String name_salary = (String)tuple.getValues().get(0);
        		String[] arr = name_salary.split("_");
        		String name = arr[0];
        		Integer salary = Integer.parseInt(arr[1]);
        		if(salary > 25000)
        			writer.write(name + "'s salary "+ arr[1] + " is greater then 25000");
            writer.newLine();
            writer.flush();
        	}catch(Exception e){}
        }

    }
